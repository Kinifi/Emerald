/**
 * Created by chrisfigueroa on 12/22/15.
 */


/**
 * the game object that will create a canvas on the default HTML file
 * Collision should be used with frame width
 * @function
 * @param {int} x - the x position on the canvas
 * @param {int} y - the y position on the canvas
 * @param {int} framewidth - the width of each animation frame
 * @param {int} frameheight - the height of each animation frame
 * @param {int} srcwidth - the width of the sprite you want to render
 * @param {int} srcheight - the height of the sprite you want to render
 * @param {int} numframes - the number of frames the animation contains
 * @param {int} speed - the speed of the animation. Higher the number the better
 */
var animsprite = function (x, y, framewidth, frameheight, srcwidth, srcheight, numframes, speed) {

    //the file location
    this.src = "";

    this.x = x;

    this.y = y;

    this.animationspeed = speed || 50;

    //x coordinate in the canvas
    this.srcwidth = srcwidth;

    //y coordinate in the canvas
    this.srcheight = srcheight;

    //width of the sprite
    this.framewidth = framewidth;

    //height of the image
    this.frameheight = framewidth;

    //number of frames
    this.numberofframes = numframes;

    this.currentframe = 0;

    this.currentanimationframeplacement = 0;

    var animTick = 0;

    this.draw = function () {

        var ctx = gamecanvascontext;
        var img = new Image();
        img.src = this.src;

        //tick so we can animate
        //animation tick is updated once every frame
        animTick++;

        //animate if animTick is higher than the animation speed.
        if(animTick > this.animationspeed)
        {
            //console.log(animTick);

            this.currentanimationframeplacement = this.framewidth * this.currentframe;
            //console.log(this.currentanimationframeplacement);
            //console.log(this.currentanimationframeplacement);

            this.currentframe++;

            if (this.currentframe > this.numberofframes - 1) {
                this.currentframe = 0;
            }

            animTick = 0;
        }

        //ctx.drawImage( img, this.x, this.y, this.srcwidth, this.srcheight);
        ctx.drawImage(img, this.currentanimationframeplacement, 0, this.framewidth, this.frameheight, this.x, this.y, this.framewidth, this.frameheight);

    }

};


/**
 * Draw a sprite on the screen
 * @function
 */
var sprite = function () {

    //the file location
    this.src = "";
    //x coordinate in the canvas
    this.x = 220;
    //y coordinate in the canvas
    this.y = 270;
    //width of the sprite
    this.width = 32;
    //height of the image
    this.height = 32;

    this.draw = function () {

        var ctx = gamecanvascontext;
        var img = new Image();
        img.src = this.src;
        ctx.drawImage( img, this.x, this.y, this.width, this.height);

    }
};


/**
 * Draws a Rectangle on screen
 * @function
 */
var rect = function () {

    this.color = "#00A";
    this.x = 220;
    this.y = 270;
    this.width = 32;
    this.height = 32;

    this.draw = function() {

        var ctx = gamecanvascontext;
        ctx.fillStyle = this.color;
        ctx.fillRect(this.x, this.y, this.width, this.height);
        ctx.fill();
    }
};


/**
 * Draws a circle on screen
 * @function
 */
var circle = function () {

    this.color = "#00A";
    this.x = 220;
    this.y = 270;
    this.radius = 40;
    this.width = this.radius / 2;
    this.height = this.radius / 2;

    this.draw = function() {

        var ctx = gamecanvascontext;
        ctx.beginPath();
        ctx.fillStyle = this.color;
        ctx.arc( this.x, this.y, this.radius, 0, 2*Math.PI);
        ctx.fill();
        ctx.stroke();
    }
};


/**
 * Draws a line on screen
 * @function
 */
var line = function () {

    this.startPosx = 0;
    this.startPosy = 0;

    this.color = "#000000";

    var lines = [];

     var line = function (x,y) {
        this.x = x;
        this.y = y;
    };

    this.addline = function (x,y) {
        lines.push(new line(x,y));
    }



    this.draw = function () {


        var ctx = gamecanvascontext;

        ctx.moveTo(this.startPosx, this.startPosy);

        if(lines.length == null)
        {
            console.error('create a new line with addline(x,y); ');
        }
        else
        {
            for (i = 0; i < lines.length; i++) {
                ctx.lineTo(lines[i].x, lines[i].y);
            }
        }

        ctx.strokeStyle = this.color;
        ctx.stroke();

    }

};

