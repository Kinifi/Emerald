/**
 * Created by chrisfigueroa on 1/2/16.
 */


/**
 * Waits for a set amount of milliseconds and executes passed function
 * @function
 * @param {function} func - the function to be executed after the amount of time is done
 * @param {float} milliseconds - the amount of time in milliseconds until the func is called
 */
var afterTimer = function (func, milliseconds) {

    window.setTimeout(func, milliseconds);
};

/**
 * Cancels an afterTimer Function
 * @function
 * @param {function} func - the function to be cancelled
 * @param {function} callback - function to call after cancelling the afterTimer
 */
var cancelTimer = function (waitingtimerfunction, callback) {

    window.cancelTimer(waitingtimerfunction);
    if(callback == undefined && callback == null)
    {
        //do nothing
    }
    else
    {
        callback();
    }
};

/**
 * Waits for a set amount of milliseconds and executes passed function
 * @function
 * @param {function} func - the function to be executed after the amount of time is done
 * @param {float} milliseconds - the amount of time in milliseconds until the func is called
 */
var repeatTimer = function(func, milliseconds) {

    window.setInterval(func, milliseconds);
};

/**
 * Cancels an repeatingTimer
 * @function
 * @param {function} func - the function to be cancelled
 * @param {function} callback - function to call after cancelling the afterTimer
 */
var clearRepeatTimer = function(repeatingFunction, callback) {
    window.clearInterval(repeatingFunction);

    if(callback == undefined && callback == null)
    {
        //do nothing
    }
    else
    {
        callback();
    }
};