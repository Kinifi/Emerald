/**
 * Created by chrisfigueroa on 12/22/15.
 */


var mygame = new Game("My First Game In Emerald", 800,600, onGameComplete);

function onGameComplete () {
    console.log('Created Game Canvas');
}

setGameLoopFunctions(update, draw, post);
setGameBackgroundColor('#FFFFFF');


var testsprite = new animsprite(10, 10, 80, 90, 400, 90, 5, 20);
testsprite.src = './sprites/frame-sprite-animation.png';
testsprite.x = 100;
testsprite.y = 200;

//called once per frame
function update()
{

}


//called once per frame after update
function draw()
{
    testsprite.draw();
}

function post()
{

}