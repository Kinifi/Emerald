 [![Gitter](https://badges.gitter.im/kinifi/Emerald.svg)](https://gitter.im/kinifi/Emerald?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)
[![forthebadge](http://forthebadge.com/images/badges/uses-js.svg)](http://forthebadge.com)
[![forthebadge](http://forthebadge.com/images/badges/uses-html.svg)](http://forthebadge.com)
[![forthebadge](http://forthebadge.com/images/badges/fuck-it-ship-it.svg)](http://forthebadge.com)
[![forthebadge](http://forthebadge.com/images/badges/contains-cat-gifs.svg)](http://forthebadge.com)



# Emerald
A JS Game Framework that is simple, fun, open source, and free.


[Trello Board for Features / Tasks](https://trello.com/b/VKANLgxV/emerald-game-framework).

Its simple, fast, and used for fun. 

//Create the index.html file

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <script type='text/javascript' src="emerald.min.js"></script>
    <script type='text/javascript' src="yourscript.js"></script>
</body>
</html>
```

*STEP ONE
*create a new game object
*give it a width and height

```javascript

var mygame = new Game("My First Game In Emerald", 800,600, onGameComplete);

function onGameComplete () {
    console.log('Created Game Canvas');
}
```


*STEP TWO
*tell emerald which methods to call for the game logic

```
setGameLoopFunctions(update, draw, postupdate);
```
*STEP THREE
*Set background
```
setGameBackgroundColor('#000000');
```
